package Common_Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {

	public static File createFolder(String foldername) {

		// Step 1 : Fetch the current Java project

		String projectFolder = System.getProperty("user.dir");
		System.out.println(projectFolder);

		// Step 2 : Check if foldername coming in variable foldername already exists in
		// projectFolder and create foldername accordingly

		File folder = new File(projectFolder + "\\ApiLogs\\" + foldername);

		if (folder.exists()) {
			System.out.println(folder + " , Already exists in Java Project :" + projectFolder);
		} else {
			System.out.println(
					folder + " , Doesn't exists in Java Project :" + projectFolder + ", " + "hence creating it");
			folder.mkdir();
			System.out.println(folder + " , Created in Java Project :" + projectFolder);
		}

		return folder;

	}

	public static void createLogFile(String Filename, File Filelocation, String endpoint, String requestBody,
			String responseHeader, String responseBody) throws IOException {

		// Step 1 : Create and open a text file
		File newTextFile = new File(Filelocation + "\\" + Filename + ".txt");
		System.out.println("File create with name :" + newTextFile.getName());

		// Step 2 : Write data into the file
		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("Request body is :\n" + requestBody + "\n\n");
		writedata.write("Response header is :\n" + responseHeader + "\n\n");
		writedata.write("Response body is :\n" + responseBody);

		// Step 3 : Save and close the file
		writedata.close();

	}

	public static ArrayList<String> ReadExcelData(String sheetname, String TestCase) throws IOException {
		 ArrayList<String> arrayData = new  ArrayList<String>();
		String projectdir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(projectdir + "\\Datafiles\\InputData.xlsx");// Read file
		XSSFWorkbook wb = new XSSFWorkbook(fis);// Open the excel file
       int countofsheets = wb.getNumberOfSheets();// total no. of sheets
		//System.out.println(countofsheets);
		for (int i = 0; i < countofsheets; i++) {
			if (wb.getSheetName(i).equals(sheetname)) {
				//System.out.println(wb.getSheetName(i));

				XSSFSheet sheet = wb.getSheetAt(i); // get desired sheet from the workbook

				Iterator<Row> rows = sheet.iterator(); // Iterate through each row one by one

				// until there is element condition holds true
				while (rows.hasNext()) {
					Row datarows = rows.next(); // it will go to row2 from row1
					String testcasename = datarows.getCell(0).getStringCellValue();// it gives testcase name
					// System.out.println(testcasename);
					if (testcasename.equals(TestCase)) { // if the testcse is as required
						 Iterator<Cell> cellvalues = datarows.iterator(); // Apply the iterator on cell value
						while (cellvalues.hasNext()) { // until there is cellvaluues in the row
						 	Cell cell = cellvalues.next(); // it will go to cell2 from cell1 in row
							CellType datatype=cell.getCellType();
							String testdata="";
							if(datatype.toString().equals("STRING")) {  
								 testdata = cell.getStringCellValue();
								}
							else if(datatype.toString().equals("NUMERIC")) {
							Double num_testdata =cell.getNumericCellValue();
	   						testdata=String.valueOf(num_testdata);}
							System.out.println(testdata);
							arrayData.add(testdata);
						}
						break;//all cell values in row getting
					}
				}
				break;//we get the desired data on specific row
			} else {
				System.out.println("No sheet found of name : " + sheetname + " in current iteration : " + i);
			}
		}
		wb.close();
		return arrayData;
	}
}