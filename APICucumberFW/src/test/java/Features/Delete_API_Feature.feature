Feature: Trigger the Delete API

@deleteapi
Scenario: Trigger the Delete API with valid request body parameter
     Given enter the end point
     When Send the delete request with payload
     Then validate StatusCode
   