Feature: Trigger the Get API

@getapi
Scenario: Trigger the API request with valid request body parameters
    Given Enter the end point
    When send the request with payload
    Then validate status code 
    And the response body should contain a list of users
  


