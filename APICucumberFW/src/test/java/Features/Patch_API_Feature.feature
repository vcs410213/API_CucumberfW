Feature: Trigger the PATCH API with required parameters

@patchapi
Scenario: Trigger the API request with valid request_body parameters
    Given Enter NAME and JOB in request_body
    When send the Patch request with payload
    Then Validate status_code 
    And Validate response_body parameters
  
@patchapi  
Scenario Outline: Trigger the API request with valid request_body parameters
    Given Enter "<NAME>" and "<JOB>" in request_body
    When send the Patch request with payload
    Then Validate status_code 
    And Validate response_body parameters
    
Examples:
    |NAME|JOB|  
    |Sham|tester|
    |raghav|engineer| 


