package stepDefinitions;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import EnviornmentandRepository.Enviornment;
import EnviornmentandRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_StepDefi {
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;

	
	@Given("enter the end point")
	public void enter_the_end_point() {
		
		endpoint = Enviornment.post_endpoint();
	}
	@When("Send the delete request with payload")
	public void send_the_delete_request_with_payload() {
		response = API_Trigger.Delete_API_Trigger(requestbody, endpoint);

	    
	}
	@Then("validate StatusCode")
	public void validate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");
	    
	}
	



}
