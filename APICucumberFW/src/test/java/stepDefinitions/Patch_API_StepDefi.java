package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import EnviornmentandRepository.Enviornment;
import EnviornmentandRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_API_StepDefi {
	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	
	@Given("Enter NAME and JOB in request_body")
	public void enter_name_and_job_in_request_body() {
		requestbody = RequestRepository.patch_request_body();
		endpoint = Enviornment.patch_endpoint();
	}
	@Given("Enter {string} and {string} in request_body")
	public void enter_and_in_request_body(String string, String string2) {
		 requestbody ="{\r\n"+ "    \"name\": \""+string+"\",\r\n"+ "    \"job\": \""+string2+"\"\r\n"+ "}";
		 endpoint = Enviornment.patch_endpoint();
	}
	@When("send the Patch request with payload")
	public void send_the_patch_request_with_payload() {
		response = API_Trigger.Patch_API_Trigger(requestbody, endpoint);

	   
	}

	@Then("Validate status_code")
	public void validate_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
	    
	}
	@Then("Validate response_body parameters")
	public void validate_response_body_parameters() {
		
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		
		
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	   
	}

