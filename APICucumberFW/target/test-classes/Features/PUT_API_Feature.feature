Feature: Trigger the PUT API with required parameters


@putapi
Scenario: Trigger the API request with valid requestbody parameters
    Given Enter NAME and JOB in PUT request body
    When Send the PUT request with payload
    Then Validate statuscode
    And Validate responsebody parameters
  
@putapi  
Scenario Outline: Trigger the API request with valid requestbody parameters
    Given Enter "<NAME>" and "<JOB>" in requestbody
    When Send the PUT request with payload
    Then Validate statuscode
    And Validate responsebody parameters
  
Examples:
    |NAME|JOB|  
    |abhi|tester|
    |raghav|engineer| 
  